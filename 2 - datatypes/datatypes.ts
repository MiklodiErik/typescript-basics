// number
let num: number = 10;

// string
let str = "this is a string";

// boolean
let bool = false;

// union
let uni: number | string = "10";

// literal type
let literal: 1 | 2 | 3 | 4 | 5 = 5;
let errorMessage: "NoPermission" | "InvalidValue" = "InvalidValue";

// type alias
type httpErrorCodes = 200 | 404 | 500;
let httpCode: httpErrorCodes = 404;

type AllowedNumbers = 10 | 20 | 30;
const add = (num1: AllowedNumbers, num2: AllowedNumbers) => num1 + num2;
add(10, 20);

// arrays
let numbers: Array<number> = [1, 2, 3, 4, 5];
//numbers.push("this is a string");     // error

// tuples - fixed length
let person: [string, number] = ["John", 32];

// array desctructuring
const [fname, age] = person;
console.log(fname);
console.log(age);

const enum Skill {
  HTML = "HTML",
  CSS = "CSS",
  JS = "JS",
  TS = "TS",
}

type Person = {
  name: string;
  age: number;
  email: string;
  phone?: string; // optional property
  skills: Array<Skill>;
  level: SenorityLevel;
};

let user1: Person = {
  name: "John",
  age: 32,
  email: "john@gmail.com",
  skills: [Skill.HTML, Skill.CSS, Skill.JS, Skill.TS],
  level: SenorityLevel.Medior,
};

console.log(user1);

// Enum
const enum SenorityLevel {
  Junior = "Junior",
  Medior = "Medior",
  Senior = "Senior",
  Expert = "Expert",
}

let user2: SenorityLevel = SenorityLevel.Medior;
