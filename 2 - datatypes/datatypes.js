"use strict";
// number
let num = 10;
// string
let str = "this is a string";
// boolean
let bool = false;
// union
let uni = "10";
// literal type
let literal = 5;
let errorMessage = "InvalidValue";
let httpCode = 404;
const add = (num1, num2) => num1 + num2;
add(10, 20);
// arrays
let numbers = [1, 2, 3, 4, 5];
//numbers.push("this is a string");     // error
// tuples - fixed length
let person = ["John", 32];
// array desctructuring
const [fname, age] = person;
console.log(fname);
console.log(age);
let user1 = {
    name: "John",
    age: 32,
    email: "john@gmail.com",
    skills: ["HTML" /* Skill.HTML */, "CSS" /* Skill.CSS */, "JS" /* Skill.JS */, "TS" /* Skill.TS */],
    level: "Medior" /* SenorityLevel.Medior */,
};
console.log(user1);
let user2 = "Medior" /* SenorityLevel.Medior */;
