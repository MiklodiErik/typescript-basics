const enum DamagedPart {
  FrontLeftDoor = "Front Left Door",
  FrontRightDoor = "Front Right Door",
  RearLeftDoor = "Rear Left Door",
  RearRightDoor = "Rear Right Door",
  Hood = "Hood",
  Engine = "Engine",
  Chassis = "Chassis",
}
const enum DamageRate {
  Low = "Low",
  Medium = "Medium",
  High = "High",
}
type Damage = {
  rate: DamageRate;
  part: DamagedPart;
};
const enum VehicleClass {
  Bike = "Bike",
  Car = "Car",
}
type Vehicle = {
  vehicleClass: VehicleClass;
  brand: string;
  type: string;
  year: number;
  damages: Array<Damage>;
};

export { Vehicle, VehicleClass, Damage, DamageRate, DamagedPart };
