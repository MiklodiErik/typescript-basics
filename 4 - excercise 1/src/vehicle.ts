import {
  DamageRate,
  DamagedPart,
  Vehicle,
  VehicleClass,
} from "./datastructures";

let newCar: Vehicle = {
  vehicleClass: VehicleClass.Bike,
  brand: "Mazda",
  type: "323f",
  year: 1995,
  damages: [
    {
      rate: DamageRate.Medium,
      part: DamagedPart.FrontRightDoor,
    },
    {
      rate: DamageRate.Low,
      part: DamagedPart.Hood,
    },
  ],
};

console.log(newCar);

/* feladat:
1. Írj olyan függvényt amely hozzáad egy új járművet(Vehicle) a járműparkhoz(Array<Vehicle>).
2. Egészítsd ki a struktúrát color tulajdonsággal, melybe tárolj le 5 színt enum segítségével.
3. Sikeres bevitel esetén a függvény, adjon vissza egy üzenetet, erre használj megfelelő return type-ot. (lehet saját típus is)
*/
