"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let newCar = {
    vehicleClass: "Bike" /* VehicleClass.Bike */,
    brand: "Mazda",
    type: "323f",
    year: 1995,
    damages: [
        {
            rate: "Medium" /* DamageRate.Medium */,
            part: "Front Right Door" /* DamagedPart.FrontRightDoor */,
        },
        {
            rate: "Low" /* DamageRate.Low */,
            part: "Hood" /* DamagedPart.Hood */,
        },
    ],
};
console.log(newCar);
