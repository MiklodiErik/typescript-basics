"use strict";
function add(num1, num2, num3) {
    if (num3) {
        return num1 + num2 + num3;
    }
    return num1 + num2;
}
console.log(add(10, 1));
// arrow function
const multiply = (num1, num2) => num1 * num2 > 0 ? num1 * num2 : "less than 0";
console.log(multiply(10, 12));
// optional parameter
const divide = (num1, num2) => num2 ? num1 / num2 : num1 / 2;
console.log(divide(10));
// default value
const subtract = (num1, num2 = 2) => num1 - num2;
console.log(subtract(10));
