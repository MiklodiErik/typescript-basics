enum ErrorType {
  NotFound = "Not Found",
  PermissionDenied = "Permission Denied",
  InvalidLength = "Invalid Length",
}
type ErrorMessage = {
  type: ErrorType;
  message: string;
};

type SearchSummary = {
  searchResults: Array<string>;
  match: number;
};

const search = (
  query: string,
  authenticated = false
): SearchSummary | ErrorMessage => {
  // auth check
  if (!authenticated) {
    return {
      type: ErrorType.PermissionDenied,
      message: "Please sign in first!",
    };
  }
  // check query length
  if (query.length < 3) {
    return {
      type: ErrorType.InvalidLength,
      message: "Query must be at least 3 characters long!",
    };
  }

  const searchResults: Array<string> = [];
  database.map((item) =>
    item.includes(query) ? searchResults.push(item) : null
  );

  // result length check
  if (searchResults.length === 0) {
    return {
      type: ErrorType.NotFound,
      message: `No matches found for search: '${query}'`,
    };
  }
  return {
    searchResults,
    match: searchResults.length,
  };
};

const database: Array<string> = [
  "apple",
  "banana",
  "orange",
  "pineapple",
  "grape",
  "grapefruit",
  "melon",
  "watermelon",
  "peach",
  "lemon",
];

console.log(search("lem", true));
