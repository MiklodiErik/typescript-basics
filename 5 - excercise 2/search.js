"use strict";
var ErrorType;
(function (ErrorType) {
    ErrorType["NotFound"] = "Not Found";
    ErrorType["PermissionDenied"] = "Permission Denied";
    ErrorType["InvalidLength"] = "Invalid Length";
})(ErrorType || (ErrorType = {}));
const search = (query, authenticated = false) => {
    // auth check
    if (!authenticated) {
        return {
            type: ErrorType.PermissionDenied,
            message: "Please sign in first!",
        };
    }
    // check query length
    if (query.length < 3) {
        return {
            type: ErrorType.InvalidLength,
            message: "Query must be at least 3 characters long!",
        };
    }
    const searchResults = [];
    database.map((item) => item.includes(query) ? searchResults.push(item) : null);
    // result length check
    if (searchResults.length === 0) {
        return {
            type: ErrorType.NotFound,
            message: `No matches found for search: '${query}'`,
        };
    }
    return {
        searchResults,
        match: searchResults.length,
    };
};
const database = [
    "apple",
    "banana",
    "orange",
    "pineapple",
    "grape",
    "grapefruit",
    "melon",
    "watermelon",
    "peach",
    "lemon",
];
console.log(search("lem", true));
